import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth/auth.guard';
import { ErrorsPageComponent } from './shared/components/errors-page/errors-page.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/login-page/login-page.module').then(m => m.LoginPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./features/main-page/main-page.module').then(m => m.MainPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: ErrorsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
