import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterModule } from '@angular/router';
import { ErrorsPageComponent } from './components/errors-page/errors-page.component';
import { HeaderComponent } from './components/header/header.component';
import { FilterByPipe } from './pipes/filter-by/filter-by.pipe';

const components = [HeaderComponent, ErrorsPageComponent, FilterByPipe];

@NgModule({
  declarations: [...components],
  exports: [...components],
  imports: [CommonModule, FormsModule, RouterModule, MatSnackBarModule]
})
export class SharedModule {}
