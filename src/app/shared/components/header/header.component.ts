import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from '../../models/menu-item.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public menuExpanded: boolean;

  @Input() appTitle = '';
  @Input() menuItems: Array<MenuItem> = [];

  constructor() {}

  ngOnInit(): void {}
}
