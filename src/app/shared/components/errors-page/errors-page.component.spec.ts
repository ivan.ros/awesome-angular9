import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { ErrorsPageComponent } from './errors-page.component';

describe('GIVEN ErrorsPageComponent', () => {
  let component: ErrorsPageComponent;
  let fixture: ComponentFixture<ErrorsPageComponent>;

  class RouterStub {
    navigate() {}
  }

  class ActivatedRouteStub {
    snapshot = {
      queryParams: {}
    };
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorsPageComponent],
      imports: [RouterTestingModule, CommonModule],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SHOULD should create', () => {
    expect(component).toBeTruthy();
  });
});
