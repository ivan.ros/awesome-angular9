import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-errors-page',
  templateUrl: './errors-page.component.html',
  styleUrls: ['./errors-page.component.scss']
})
export class ErrorsPageComponent implements OnInit {
  public status: string;
  public message: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.status = this.activatedRoute.snapshot.queryParams.status || '404';
    this.message = this.activatedRoute.snapshot.queryParams.message;
  }

  goToHomePage() {
    this.router.navigate(['/main']);
  }
}
