export const CONSTANTS = {
  MINIONS: {
    STATUS: {
      WORKING: 'Working',
      HAVING_FUN: 'Having fun',
      EATING: 'Eating',
      SLEEPING: 'Sleeping'
    }
  }
};
