import minionsData from '../../../../stubbs/minions/minions.json';
import { FilterByPipe } from './filter-by.pipe';

describe('GIVEN FilterByPipe', () => {
  let pipe: FilterByPipe;

  beforeEach(() => {
    pipe = new FilterByPipe();
  });

  describe('WHEN there are not valid list', () => {
    it('SHOULD not filter retrieving same list', () => {
      const list = null;
      const result = pipe.transform(list, '');

      expect(result).toEqual(list);
    });
  });

  describe('WHEN there are not valid list length', () => {
    it('SHOULD not filter retrieving same list', () => {
      const list = [];
      const result = pipe.transform(list, '');

      expect(result).toEqual(list);
    });
  });

  describe('WHEN there are not valid text', () => {
    it('SHOULD not filter retrieving same list', () => {
      const list = ['a', 'b', 'c'];
      const result = pipe.transform(list, '');

      expect(result).toEqual(list);
    });
  });

  describe('WHEN there are valid params', () => {
    it('SHOULD NOT filter minions by any text', () => {
      const text = 'bob';
      const result = pipe.transform(minionsData.elements, text);

      expect(result.length).toEqual(2);
    });

    it('SHOULD filter minions by "bob" text in default property', () => {
      const text = 'bob';
      const result = pipe.transform(minionsData.elements, text);

      expect(result.length).toEqual(2);
    });

    it('SHOULD filter minions by "bob" text in Name property', () => {
      const text = 'bob';
      const properties = ['Name'];
      const result = pipe.transform(minionsData.elements, text, properties);

      expect(result.length).toEqual(2);
    });

    it('SHOULD filter minions by "work" text in all properties', () => {
      const text = 'work';
      const minionProperties = Object.keys(minionsData.elements[0]);
      const result = pipe.transform(minionsData.elements, text, minionProperties);

      expect(result.length).toEqual(5);
    });
  });
});
