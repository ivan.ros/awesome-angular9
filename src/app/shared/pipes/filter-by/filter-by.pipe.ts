import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBy',
  pure: false
})
export class FilterByPipe implements PipeTransform {
  transform(list: Array<any>, text: string, filterBy?: Array<string>): any {
    if (!list || !list.length || !text) {
      return list;
    }

    text = text.toLowerCase();

    if (!filterBy) {
      return list.filter(item => item.Name.toLowerCase().includes(text));
    } else {
      return list.filter(item =>
        filterBy.some((key: string) => {
          const propertyText = item[key].toString().toLowerCase();
          return propertyText.includes(text);
        })
      );
    }
  }
}
