import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: `[confirmWindow]`
})
export class ConfirmWindowDirective {
  @Input() confirmWindowMessage = 'Are you sure you want to do this?';
  @Input() confirmWindow = () => {};

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('click', ['$event'])
  showConfirmWindow() {
    const confirmed = window.confirm(this.confirmWindowMessage);

    if (confirmed) {
      this.confirmWindow();
    }
  }

  @HostListener('mouseenter', ['$event'])
  showTextQuestionMark() {
    this.renderer.addClass(this.elementRef.nativeElement, 'font-italic');
  }

  @HostListener('mouseleave', ['$event'])
  hideTextQuestionMark() {
    this.renderer.removeClass(this.elementRef.nativeElement, 'font-italic');
  }
}
