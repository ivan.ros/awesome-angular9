import { Minion } from './minion.model';
import { Villain } from './villain.model';

export class Team {
  Id: number;
  Name: string;
  Description: string;
  CreatedOn: string;
  Chief: Villain;
  Minions: Array<Minion>;
}
