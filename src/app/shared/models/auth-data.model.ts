export class AuthData {
  username: string;
  email: string;
  token: string;
}
