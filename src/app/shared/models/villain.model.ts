export class Villain {
  Id: number;
  Name: string;
  Title: string;
  Picture: string;
}
