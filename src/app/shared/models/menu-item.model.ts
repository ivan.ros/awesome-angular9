export class MenuItem {
  id: string;
  name: string;
  route: string;
}
