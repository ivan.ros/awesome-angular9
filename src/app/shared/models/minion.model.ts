export class Minion {
  Id: number;
  Name: string;
  Gender: string;
  NumberOfEyes: number;
  Picture: string;
  IsFriendly: boolean;
  Status: string;
  Skills: Array<string>;
  Vocabulary: Array<string>;
}
