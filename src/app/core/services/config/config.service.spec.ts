import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ConfigService } from './config.service';

describe('GIVEN ConfigService', () => {
  let service: ConfigService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({});
  });

  beforeEach(() => {
    service = TestBed.inject(ConfigService);
  });

  it('SHOULD should be created', () => {
    expect(service).toBeTruthy();
  });
});
