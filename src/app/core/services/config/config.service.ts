import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl + '/api';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  // Endpoints
  api = {
    auth: {
      root: `${apiUrl}/auth`
    },
    teams: {
      root: `${apiUrl}/teams`,
      minions: `${apiUrl}/teams/:teamId/minions`
    }
  };

  constructor() {}
}
