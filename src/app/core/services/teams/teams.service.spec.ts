import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ContactForm } from 'src/app/shared/models/contact-form.model';
import minionsData from '../../../../stubbs/minions/minions.json';
import errorData from '../../../../stubbs/others/400.json';
import teamsData from '../../../../stubbs/teams/teams.json';
import { ConfigService } from '../config/config.service';
import { TeamsService } from './teams.service';

describe('GIVEN TeamsService', () => {
  let service: TeamsService;

  const contactData: ContactForm = {
    Villain: 'villain',
    Team: 'team villain',
    Email: 'villain@team.com',
    Password: 'villain123',
    Comments: 'Example request'
  };

  class ConfigServiceStub {
    api = {
      teams: {
        root: `/teams`,
        minions: `/teams/:teamId/minions`
      }
    };
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: ConfigService, useClass: ConfigServiceStub }]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(TeamsService);
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('SHOULD call available teams GET service with success status', inject(
    [HttpTestingController, TeamsService, ConfigService],
    (httpMock: HttpTestingController, teamsService: TeamsService, configService: ConfigService) => {
      teamsService.getTeams().subscribe(data => {
        expect(data.length).toBe(teamsData.elements.length);
      });

      const req = httpMock.expectOne(configService.api.teams.root);
      expect(req.request.method).toEqual('GET');

      req.flush(teamsData);
    }
  ));

  it('SHOULD call available teams GET service with error status', inject(
    [HttpTestingController, TeamsService, ConfigService],
    (httpMock: HttpTestingController, teamsService: TeamsService, configService: ConfigService) => {
      teamsService.getTeams().subscribe(
        data => {},
        error => {
          expect(error.error).toBe(errorData);
        }
      );

      const req = httpMock.expectOne(configService.api.teams.root);
      expect(req.request.method).toEqual('GET');

      req.flush(errorData, { status: 400, statusText: 'Bad request' });
    }
  ));

  it('SHOULD call available team minions GET service with success status', inject(
    [HttpTestingController, TeamsService, ConfigService],
    (httpMock: HttpTestingController, teamsService: TeamsService, configService: ConfigService) => {
      const teamId = 1;
      teamsService.getTeamMinions(teamId).subscribe(data => {
        expect(data.length).toBe(minionsData.elements.length);
      });

      const req = httpMock.expectOne(configService.api.teams.minions.replace(':teamId', teamId.toString()));
      expect(req.request.method).toEqual('GET');

      req.flush(minionsData);
    }
  ));

  it('SHOULD call available team minions GET service with error status', inject(
    [HttpTestingController, TeamsService, ConfigService],
    (httpMock: HttpTestingController, teamsService: TeamsService, configService: ConfigService) => {
      const teamId = 1;
      teamsService.getTeamMinions(teamId).subscribe(
        data => {},
        error => {
          expect(error.error).toBe(errorData);
        }
      );

      const req = httpMock.expectOne(configService.api.teams.minions.replace(':teamId', teamId.toString()));
      expect(req.request.method).toEqual('GET');

      req.flush(errorData, { status: 400, statusText: 'Bad request' });
    }
  ));

  it('SHOULD call available contact POST service with success status', inject(
    [HttpTestingController, TeamsService, ConfigService],
    (httpMock: HttpTestingController, teamsService: TeamsService, configService: ConfigService) => {
      teamsService.contactTeam(contactData).subscribe(data => {
        expect(data).toBe({});
      });

      const req = httpMock.expectOne(configService.api.teams.root);
      expect(req.request.method).toEqual('POST');

      req.flush({});
    }
  ));

  it('SHOULD call available contact POST service with error status', inject(
    [HttpTestingController, TeamsService, ConfigService],
    (httpMock: HttpTestingController, teamsService: TeamsService, configService: ConfigService) => {
      teamsService.contactTeam(contactData).subscribe(
        data => {},
        error => {
          expect(error.error).toBe(errorData);
        }
      );

      const req = httpMock.expectOne(configService.api.teams.root);
      expect(req.request.method).toEqual('POST');

      req.flush(errorData, { status: 400, statusText: 'Bad request' });
    }
  ));
});
