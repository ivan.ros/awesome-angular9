import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { ContactForm } from 'src/app/shared/models/contact-form.model';
import { Minion } from 'src/app/shared/models/minion.model';
import { Team } from 'src/app/shared/models/team.model';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  constructor(private http: HttpClient, private config: ConfigService) {}

  getTeams(): Observable<Array<Team>> {
    return this.http.get<{ elements: Array<Team> }>(this.config.api.teams.root).pipe(
      map(response => response.elements),
      delay(1500)
    );
  }

  getTeamMinions(teamId: number): Observable<Array<Minion>> {
    return this.http
      .get<{ elements: Array<Minion> }>(this.config.api.teams.minions.replace(':teamId', teamId.toString()))
      .pipe(
        map(response => response.elements),
        delay(1500)
      );
  }

  contactTeam(body: ContactForm): Observable<any> {
    return this.http.post<any>(this.config.api.teams.root, body).pipe(delay(1500));
  }
}
