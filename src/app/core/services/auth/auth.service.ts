import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { AuthData } from 'src/app/shared/models/auth-data.model';
import { AuthForm } from 'src/app/shared/models/auth-form.model';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private config: ConfigService) {}

  auth(login: AuthForm): Observable<any> {
    return this.http.post<any>(this.config.api.auth.root, login).pipe(
      map(response => this.setAuthData(response)),
      delay(1500)
    );
  }

  isLoggedIn(): boolean {
    const authData = this.getAuthData();
    return authData && authData.token && authData.token !== '';
  }

  getAuthData(): AuthData {
    const authData = localStorage.getItem('auth_data');
    return JSON.parse(authData);
  }

  setAuthData(login: AuthData) {
    localStorage.setItem('auth_data', JSON.stringify(login));
  }
}
