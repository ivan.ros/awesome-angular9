import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ConfigService } from '../config/config.service';
import { AuthService } from './auth.service';

describe('GIVEN AuthService', () => {
  let service: AuthService;

  class ConfigServiceStub {
    api = {
      auth: {
        root: `/auth`
      }
    };
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: ConfigService, useClass: ConfigServiceStub }]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(AuthService);
  });

  it('SHOULD should be created', () => {
    expect(service).toBeTruthy();
  });
});
