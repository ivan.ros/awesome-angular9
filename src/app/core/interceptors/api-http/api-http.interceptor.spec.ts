import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { configureTestSuite } from 'ng-bullet';
import badRequestData from '../../../../stubbs/others/400.json';
import serverDownData from '../../../../stubbs/others/500.json';
import { ApiHttpInterceptor } from './api-http.interceptor';

describe('GIVEN ApiHttpInterceptor', () => {
  class RouterStub {
    navigate() {}
  }

  class MatSnackBarStub {
    open() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ApiHttpInterceptor,
          multi: true
        },
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('SHOULD handle error status 400 as snackbar alert', inject(
    [HttpClient, HttpTestingController, MatSnackBar],
    (http: HttpClient, httpMock: HttpTestingController, snackBar: MatSnackBar) => {
      spyOn(snackBar, 'open');

      http.get('/api').subscribe(
        response => {},
        e => expect(e).toBeTruthy()
      );

      const req = httpMock.expectOne('/api');

      req.flush(badRequestData, { status: 400, statusText: 'Bad Request' });

      expect(snackBar.open).toHaveBeenCalled();
    }
  ));

  it('SHOULD handle error status 404 as error page redirection', inject(
    [HttpClient, HttpTestingController, Router],
    (http: HttpClient, httpMock: HttpTestingController, router: Router) => {
      spyOn(router, 'navigate');

      http.get('/api').subscribe(
        response => {},
        e => expect(e).toBeTruthy()
      );

      const req = httpMock.expectOne('/api');

      req.flush({}, { status: 404, statusText: 'Not Found' });

      expect(router.navigate).toHaveBeenCalled();
    }
  ));

  it('SHOULD handle error status 500 as error page redirection', inject(
    [HttpClient, HttpTestingController, Router],
    (http: HttpClient, httpMock: HttpTestingController, router: Router) => {
      spyOn(router, 'navigate');

      http.get('/api').subscribe(
        response => {},
        e => expect(e).toBeTruthy()
      );

      const req = httpMock.expectOne('/api');

      req.flush(serverDownData, { status: 500, statusText: 'Server error' });

      expect(router.navigate).toHaveBeenCalled();
    }
  ));
});
