import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiHttpInterceptor implements HttpInterceptor {
  constructor(private snackBar: MatSnackBar, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((response: HttpErrorResponse) => {
        const {
          status,
          error: { message }
        } = response;
        switch (status) {
          case 404:
          case 500:
            this.router.navigate(['error'], { queryParams: { status, message } });
            break;
          default:
            this.handleErrorMessage(message);
        }

        return throwError(response);
      })
    );
  }

  private handleErrorMessage(message: string) {
    this.snackBar.open(message, null, { duration: 3000 });
  }
}
