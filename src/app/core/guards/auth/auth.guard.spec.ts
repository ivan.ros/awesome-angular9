import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { configureTestSuite } from 'ng-bullet';
import { AuthService } from '../../services/auth/auth.service';
import { AuthGuard } from './auth.guard';

describe('GIVEN AuthGuard', () => {
  let guard: AuthGuard;

  class AuthServiceStub {
    isLoggedIn() {}
  }

  class RouterStub {
    navigate() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthService, useClass: AuthServiceStub },
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  beforeEach(() => {
    guard = TestBed.inject(AuthGuard);
  });

  it('SHOULD should be created', () => {
    expect(guard).toBeTruthy();
  });
});
