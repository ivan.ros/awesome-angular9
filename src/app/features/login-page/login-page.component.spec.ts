import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { LoginPageComponent } from './login-page.component';

describe('GIVEN LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;

  class AuthServiceStub {
    auth() {}
  }

  class MatSnackBarStub {
    open() {}
  }

  class RouterStub {
    navigate() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPageComponent],
      imports: [RouterTestingModule, CommonModule, FormsModule, ReactiveFormsModule, MatSnackBarModule],
      providers: [
        { provide: AuthService, useClass: AuthServiceStub },
        { provide: MatSnackBar, useClass: MatSnackBarStub },
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SHOULD should create', () => {
    expect(component).toBeTruthy();
  });
});
