import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { SharedModule } from 'src/app/shared/shared.module';
import { MainPageComponent } from './main-page.component';

describe('GIVEN MainPageComponent', () => {
  let component: MainPageComponent;
  let fixture: ComponentFixture<MainPageComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [MainPageComponent],
      imports: [RouterTestingModule, SharedModule]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SHOULD should create', () => {
    expect(component).toBeTruthy();
  });
});
