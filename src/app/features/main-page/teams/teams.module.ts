import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { TeamMembersModule } from './team-members/team-members.module';
import { TeamsComponent } from './teams.component';

const routes = [
  {
    path: '',
    component: TeamsComponent
  }
];

const components = [TeamsComponent];

@NgModule({
  declarations: [...components],
  exports: [...components],
  imports: [RouterModule.forChild(routes), CommonModule, FormsModule, SharedModule, TeamMembersModule]
})
export class TeamsModule {}
