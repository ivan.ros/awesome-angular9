import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { configureTestSuite } from 'ng-bullet';
import minionsData from '../../../../../stubbs/minions/minions.json';
import { MemberInfoDialogComponent } from './member-info-dialog.component';

describe('GIVEN MemberInfoDialogComponent', () => {
  let component: MemberInfoDialogComponent;
  let fixture: ComponentFixture<MemberInfoDialogComponent>;

  const MAT_DIALOG_DATA_STUB = {
    ...minionsData.elements[0]
  };

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [MemberInfoDialogComponent],
      imports: [CommonModule, FormsModule, MatDialogModule],
      providers: [{ provide: MAT_DIALOG_DATA, useValue: MAT_DIALOG_DATA_STUB }]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SHOULD should create', () => {
    expect(component).toBeTruthy();
  });
});
