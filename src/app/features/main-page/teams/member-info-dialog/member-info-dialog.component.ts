import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-member-info-dialog',
  templateUrl: './member-info-dialog.component.html',
  styleUrls: ['./member-info-dialog.component.scss']
})
export class MemberInfoDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {}
}
