import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationExtras, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { TeamsService } from 'src/app/core/services/teams/teams.service';
import { CONSTANTS } from 'src/app/shared/constants/constants';
import { Minion } from 'src/app/shared/models/minion.model';
import { Team } from 'src/app/shared/models/team.model';
import { Villain } from 'src/app/shared/models/villain.model';
import { MemberInfoDialogComponent } from '../member-info-dialog/member-info-dialog.component';

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit {
  public constants = CONSTANTS;
  public minionsLoading: boolean;
  public minionFilter: string;
  public statusFilter: string;

  @Input() team: Team;
  @Output() minionsLoaded: EventEmitter<any> = new EventEmitter();

  constructor(private teamsService: TeamsService, private dialog: MatDialog, private router: Router) {}

  ngOnInit(): void {
    this.getTeamMinions(this.team);
  }

  public openMemberInfoDialog(member: Villain | Minion) {
    this.dialog.open(MemberInfoDialogComponent, {
      width: '400px',
      data: { ...member }
    });
  }

  public goToContactPage() {
    const navigationExtras: NavigationExtras = {
      queryParams: { villain: this.team.Chief.Name, team: this.team.Name }
    };

    this.router.navigate(['/main/contact'], navigationExtras);
  }

  private getTeamMinions(team: Team) {
    this.minionsLoading = true;
    this.teamsService
      .getTeamMinions(team.Id)
      .pipe(finalize(() => (this.minionsLoading = false)))
      .subscribe(minions => {
        team.Minions = minions;
        this.minionsLoaded.emit({ minions });
      });
  }
}
