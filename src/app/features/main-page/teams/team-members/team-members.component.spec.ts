import { CommonModule } from '@angular/common';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs';
import { TeamsService } from 'src/app/core/services/teams/teams.service';
import { SharedModule } from 'src/app/shared/shared.module';
import minionsData from '../../../../../stubbs/minions/minions.json';
import teamsData from '../../../../../stubbs/teams/teams.json';
import { TeamMembersComponent } from './team-members.component';

describe('GIVEN TeamMembersComponent', () => {
  let component: TeamMembersComponent;
  let fixture: ComponentFixture<TeamMembersComponent>;

  class TeamsServiceStub {
    getTeamMinions() {}
  }

  class MatDialogStub {
    open() {}
  }

  class RouterStub {
    navigate() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [TeamMembersComponent],
      imports: [
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        MatDialogModule,
        MatSelectModule,
        MatInputModule,
        SharedModule
      ],
      providers: [
        { provide: TeamsService, useClass: TeamsServiceStub },
        { provide: MatDialog, useClass: MatDialogStub },
        { provide: Router, useClass: RouterStub }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamMembersComponent);
    component = fixture.componentInstance;
    component.team = teamsData.elements[0];
  });

  describe('WHEN team minions data is provided', () => {
    beforeEach(inject([TeamsService], (teamsService: TeamsService) => {
      spyOn(teamsService, 'getTeamMinions').and.returnValue(of(minionsData.elements));
      fixture.detectChanges();
    }));

    it('SHOULD should show filter and list', () => {
      expect(component).toBeTruthy();
    });

    describe('AND a minion is clicked', () => {
      let eFirstMinionBox: HTMLElement;

      beforeEach(inject([MatDialog], (dialog: MatDialog) => {
        spyOn(dialog, 'open');
        const eMinionBoxes: HTMLElement = fixture.nativeElement.querySelectorAll('.minions-grid .minion-box');
        eFirstMinionBox = eMinionBoxes[0];
        eFirstMinionBox.click();
        fixture.detectChanges();
      }));

      it('SHOULD open minion info dialog', inject([MatDialog], (dialog: MatDialog) => {
        expect(dialog.open).toHaveBeenCalled();
      }));
    });

    describe('AND contact button is clicked', () => {
      beforeEach(inject([Router], (router: Router) => {
        spyOn(router, 'navigate');
        const eNavigateBtn: HTMLElement = fixture.nativeElement.querySelector('.action-navigate');
        eNavigateBtn.click();
        fixture.detectChanges();
      }));

      it('SHOULD navigate to contact page', inject([Router], (router: Router) => {
        const queryParams = { villain: component.team.Chief.Name, team: component.team.Name };
        expect(router.navigate).toHaveBeenCalledWith(['/main/contact'], { queryParams });
      }));
    });
  });

  describe('WHEN team minions data is not provided', () => {
    beforeEach(inject([TeamsService], (teamsService: TeamsService) => {
      spyOn(teamsService, 'getTeamMinions').and.returnValue(of([]));
      fixture.detectChanges();
    }));

    it('SHOULD should show empty message', () => {
      expect(component).toBeTruthy();
    });
  });
});
