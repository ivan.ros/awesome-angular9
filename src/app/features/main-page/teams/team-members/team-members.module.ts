import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from 'src/app/shared/shared.module';
import { MemberInfoDialogComponent } from '../member-info-dialog/member-info-dialog.component';
import { TeamMembersComponent } from './team-members.component';

const components = [TeamMembersComponent, MemberInfoDialogComponent];

@NgModule({
  declarations: [...components],
  exports: [...components],
  imports: [CommonModule, FormsModule, MatDialogModule, MatSelectModule, MatInputModule, SharedModule]
})
export class TeamMembersModule {}
