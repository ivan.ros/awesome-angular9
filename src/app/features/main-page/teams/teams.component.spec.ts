import { CommonModule } from '@angular/common';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs';
import { TeamsService } from 'src/app/core/services/teams/teams.service';
import { SharedModule } from 'src/app/shared/shared.module';
import minionsData from '../../../../stubbs/minions/minions.json';
import teamsData from '../../../../stubbs/teams/teams.json';
import { TeamMembersModule } from './team-members/team-members.module';
import { TeamsComponent } from './teams.component';

describe('GIVEN TeamsComponent', () => {
  let component: TeamsComponent;
  let fixture: ComponentFixture<TeamsComponent>;

  class TeamsServiceStub {
    getTeams() {}
    getTeamMinions() {}
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [TeamsComponent],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        CommonModule,
        FormsModule,
        SharedModule,
        TeamMembersModule
      ],
      providers: [{ provide: TeamsService, useClass: TeamsServiceStub }]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsComponent);
    component = fixture.componentInstance;
  });

  describe('WHEN teams data is provided', () => {
    beforeEach(inject([TeamsService], (teamsService: TeamsService) => {
      spyOn(teamsService, 'getTeams').and.returnValue(of(teamsData.elements));
      fixture.detectChanges();
    }));

    it('SHOULD should show filter and list', () => {
      expect(component).toBeTruthy();
    });

    describe('AND a team is clicked', () => {
      let eFirstTeamBox: HTMLElement;

      beforeEach(inject([TeamsService], (teamsService: TeamsService) => {
        spyOn(teamsService, 'getTeamMinions').and.returnValue(of(minionsData.elements));
        const eTeamBoxes: HTMLElement = fixture.nativeElement.querySelectorAll('.teams-grid .team-box');
        eFirstTeamBox = eTeamBoxes[0];
        eFirstTeamBox.click();
        fixture.detectChanges();
      }));

      it('SHOULD select this team as current', () => {
        const eFirstTeamBoxTitle = eFirstTeamBox.querySelector('.box-item__title');
        expect(component.teamSelected.Name).toEqual(eFirstTeamBoxTitle.innerHTML);
      });

      it('SHOULD call minions service', inject([TeamsService], (teamsService: TeamsService) => {
        expect(teamsService.getTeamMinions).toHaveBeenCalled();
      }));

      describe('AND same team is clicked again', () => {
        beforeEach(inject([TeamsService], (teamsService: TeamsService) => {
          eFirstTeamBox.click();
          fixture.detectChanges();
        }));

        it('SHOULD deselect this team as current', () => {
          expect(component.teamSelected).toBeNull();
        });
      });
    });
  });

  describe('WHEN teams data is not provided', () => {
    beforeEach(inject([TeamsService], (teamsService: TeamsService) => {
      spyOn(teamsService, 'getTeams').and.returnValue(of([]));
      fixture.detectChanges();
    }));

    it('SHOULD should show empty message', () => {
      expect(component).toBeTruthy();
    });
  });
});
