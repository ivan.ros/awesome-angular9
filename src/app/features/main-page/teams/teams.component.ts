import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { TeamsService } from 'src/app/core/services/teams/teams.service';
import { Team } from 'src/app/shared/models/team.model';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  public teams: Array<Team>;
  public teamSelected: Team;
  public teamsLoading: boolean;
  public teamFilter: string;
  public minionsLoading: boolean;
  public minionFilter: string;
  public statusFilter: string;

  constructor(private teamsService: TeamsService) {}

  ngOnInit(): void {
    this.getTeams();
  }

  public onTeamClicked(team: Team) {
    if (!this.teamSelected || this.teamSelected.Id !== team.Id) {
      this.teamSelected = team;
    } else {
      this.teamSelected = null;
      this.minionFilter = '';
    }
  }

  private getTeams() {
    this.teamsLoading = true;
    this.teamsService
      .getTeams()
      .pipe(finalize(() => (this.teamsLoading = false)))
      .subscribe(teams => (this.teams = teams));
  }
}
