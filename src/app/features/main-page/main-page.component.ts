import { Component } from '@angular/core';
import { MenuItem } from 'src/app/shared/models/menu-item.model';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent {
  public menuItems: Array<MenuItem>;

  constructor() {
    this.menuItems = [
      {
        id: 'menu-home',
        name: 'Home',
        route: '/main'
      },
      {
        id: 'menu-teams',
        name: 'Teams',
        route: '/main/teams'
      },
      {
        id: 'menu-contact',
        name: 'Contact',
        route: '/main/contact'
      }
    ];
  }
}
