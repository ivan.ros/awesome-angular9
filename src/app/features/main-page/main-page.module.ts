import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MainPageComponent } from './main-page.component';
import { MainRoutingModule } from './main-routing.module';

@NgModule({
  declarations: [MainPageComponent],
  imports: [MainRoutingModule, SharedModule],
  providers: []
})
export class MainPageModule {}
