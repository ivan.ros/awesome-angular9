import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes = [
  {
    path: '',
    component: DashboardComponent
  }
];

const components = [DashboardComponent];

@NgModule({
  declarations: [...components],
  exports: [...components],
  imports: [RouterModule.forChild(routes)]
})
export class DashboardModule {}
