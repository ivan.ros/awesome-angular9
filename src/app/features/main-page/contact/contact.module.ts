import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';

const routes = [
  {
    path: '',
    component: ContactComponent
  }
];

const components = [ContactComponent];

@NgModule({
  declarations: [...components],
  exports: [...components],
  imports: [RouterModule.forChild(routes), CommonModule, ReactiveFormsModule]
})
export class ContactModule {}
