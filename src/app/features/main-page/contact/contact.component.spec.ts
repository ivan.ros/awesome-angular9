import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { configureTestSuite } from 'ng-bullet';
import { TeamsService } from 'src/app/core/services/teams/teams.service';
import { ContactComponent } from './contact.component';

describe('GIVEN ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  class TeamsServiceStub {
    contactTeam() {}
  }

  class ActivatedRouteStub {
    snapshot = {
      queryParams: {}
    };
  }

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ContactComponent],
      imports: [RouterTestingModule, CommonModule, ReactiveFormsModule],
      providers: [
        { provide: TeamsService, useClass: TeamsServiceStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SHOULD should create', () => {
    expect(component).toBeTruthy();
  });
});
