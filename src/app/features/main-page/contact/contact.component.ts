import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { TeamsService } from 'src/app/core/services/teams/teams.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public contactForm: FormGroup;
  public contactFormSubmitted: boolean;

  constructor(private teamsService: TeamsService, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    const initialData = this.activatedRoute.snapshot.queryParams;
    this.createForm(initialData);
  }

  public onSubmit() {
    this.contactFormSubmitted = false;
    this.teamsService.contactTeam(this.contactForm.value).subscribe(() => {
      this.contactFormSubmitted = true;
      this.createForm();
    });
  }

  private createForm(data: Params = {}) {
    this.contactForm = new FormGroup(
      {
        Villain: new FormControl(data.villain || '', [Validators.required, this.forbiddenNameValidator(/gru/i)]),
        Team: new FormControl(data.team || '', [Validators.required]),
        Email: new FormControl('', [Validators.required, Validators.email]),
        Password: new FormControl('', [Validators.required, Validators.minLength(6)]),
        Comments: new FormControl('', [
          Validators.required,
          Validators.minLength(20),
          Validators.pattern(/^[a-zA-Z0-9\s]*$/)
        ])
      },
      { validators: this.villainTeamNameValidator }
    );
  }

  private forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = nameRe.test(control.value.toLowerCase());
      return forbidden ? { forbiddenName: { value: control.value } } : null;
    };
  }

  private villainTeamNameValidator(formGroup: FormGroup): ValidatorFn {
    const villain = formGroup.get('Villain').value;
    const team = formGroup.get('Team').value;

    if (villain && team && team.toLowerCase().indexOf(villain.toLowerCase()) === -1) {
      formGroup.get('Team').setErrors({ villainTeamNameMatch: true });
    }
    return null;
  }
}
