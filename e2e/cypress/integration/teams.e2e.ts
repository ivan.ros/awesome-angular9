/// <reference types="Cypress" />

import { ContactPage } from '../webpages/contactPage';
import { LoginPage } from '../webpages/loginPage';
import { TeamsPage } from '../webpages/teamsPage';

describe('GIVEN Teams', () => {
  before(() => {
    cy.server();

    LoginPage.doLogin();

    cy.visit('/main/teams');
  });

  describe('WHEN contacting with team', () => {
    it('SHOULD fill Contact`s Villain and Team fields with selected team data', () => {
      const filterText = 'maleficent';
      const comments = 'I wish to take part of this team because it is awesome';
      TeamsPage.getTeamsFilter().type(filterText);
      TeamsPage.getTeamBoxByPosition(0).click();
      TeamsPage.getButtonContact().click();

      ContactPage.getInputVillain()
        .invoke('val')
        .should('not.be.empty');
      ContactPage.getInputTeam()
        .invoke('val')
        .should('not.be.empty');

      ContactPage.getInputEmail().type(Cypress.env('email'));
      ContactPage.getInputPassword().type(Cypress.env('password'));
      ContactPage.getTextareaComments().type(comments);

      ContactPage.getInputSubmit().click();

      ContactPage.getInputVillain().should('not.have.value');
      ContactPage.getInputTeam().should('not.have.value');
      ContactPage.getInputEmail().should('not.have.value');
      ContactPage.getInputPassword().should('not.have.value');
      ContactPage.getTextareaComments().should('not.have.value');
      ContactPage.getAlertSuccess().should(
        'contain.text',
        'Your comments was sent successfully, we will contact you as soon as possible!'
      );
    });
  });
});
