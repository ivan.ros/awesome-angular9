/// <reference types="Cypress" />

export class LoginPage {
  public static doLogin() {
    cy.visit('/');
    const { username, password } = Cypress.env();
    this.getInputUsername().type(username);
    this.getInputPassword().type(password);
    this.getInputSubmit().click();
  }

  private static getInputUsername() {
    return cy.get(`input#username`);
  }

  private static getInputPassword() {
    return cy.get(`input[type="password"]`);
  }

  private static getInputSubmit() {
    return cy.get(`button[type="submit"]`);
  }
}
