/// <reference types="Cypress" />

export class ContactPage {
  public static getInputVillain() {
    return cy.get(`input#villain`);
  }

  public static getInputTeam() {
    return cy.get(`input#team`);
  }

  public static getInputEmail() {
    return cy.get(`input#email`);
  }

  public static getInputPassword() {
    return cy.get(`input[type="password"]`);
  }

  public static getTextareaComments() {
    return cy.get(`textarea#comments`);
  }

  public static getInputSubmit() {
    return cy.get(`button[type="submit"]`);
  }

  public static getAlertSuccess() {
    return cy.get(`.alert.alert-success`);
  }
}
