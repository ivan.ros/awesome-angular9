/// <reference types="Cypress" />

export class TeamsPage {
  public static getTeamsFilter() {
    return cy.get(`.teams-filter`);
  }

  public static getTeamBoxByPosition(idx: number) {
    return cy.get(`.teams-grid .team-box`).eq(idx);
  }

  public static getBoxItemTitle() {
    return cy.get('.box-item__title');
  }

  public static getVillainBox() {
    return cy.get('.members-grid .villain-box');
  }

  public static getButtonContact() {
    return cy.get(`.action-navigate`);
  }
}
